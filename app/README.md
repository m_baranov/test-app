---

Методы:
* `GET /api/user` выводит список всех пользователей
* `POST /api/user` добавляет пользователя
формат json:

```
{
    'username': 'user',
    'email': 'user@host.domain',
    'password_hash': '123456'
}
```

Разворачивание:

* Установить python 3.6
* Развернуть зависимости приложения из файла `requirements.txt`
* Поправить конфигурационный файл `config.py`
* Выполнить миграции: `FLASK_APP=app.py flask db upgrade`
* Точка входа в приложение `app.py`
